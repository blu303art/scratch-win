


const KEY_BACKGROUND = 'Background'
const KEY_TITLE = 'Title'
const KEY_START = 'StartGame'
const KEY_SETTINGS = 'Settings'
const KEY_RETURN = 'Return'
const KEY_SOUND_ON = 'SoundOn'
const KEY_SOUND_OFF = 'SoundOff'
const KEY_LEADERBOARD = 'Leaderboard'
var click

class MainMenu extends Phaser.Scene {





	constructor() {

		super("MainMenu");

		this.isDown = false
		this.cover = null
		this.renderTexture = null

	}


	create() {
		click = this.sound.add('click');


		var pointer = this.input.activePointer;


		const x = 400
		const y = 300


		var bgIMG = this.add.image(x, 200, KEY_BACKGROUND)
		var Title = this.add.image(x, 200, KEY_TITLE)
		Title.setDisplaySize(400, 400)

		var SettingsButton = this.add.image(200, 385, KEY_SETTINGS)
		SettingsButton.setTint(0x686868)
		SettingsButton.setInteractive()

		var LeaderboardButton = this.add.image(600, 385, KEY_LEADERBOARD)
		LeaderboardButton.setTint(0x686868)
		LeaderboardButton.setInteractive()

		var PlayButton = this.add.image(x, 385, KEY_START)
		PlayButton.setTint(0x686868)
		PlayButton.setInteractive()


		//SettingsButton
		SettingsButton.on('pointerdown', () => this.scene.start('Settings'), click.play())
		SettingsButton.on('pointerover', function() {
			this.clearTint()
		})
		SettingsButton.on('pointerout', function() {

			this.setTint(0x686868);
		})



		//Leaderboard Button		
		LeaderboardButton.on('pointerdown', () => this.scene.start('Leaderboard'), click.play())
		LeaderboardButton.on('pointerover', function() {
			this.clearTint()
		})
		LeaderboardButton.on('pointerout', function() {

			this.setTint(0x686868);
		})


		//StartGame Button
		PlayButton.on('pointerdown', () => this.scene.start('Scene1'), ResetLives(), click.play())
		PlayButton.on('pointerover', function() {
			this.clearTint()
		})
		PlayButton.on('pointerout', function() {

			this.setTint(0x686868);
		})

	}


	preload() {

		this.load.image(KEY_BACKGROUND, 'assets/Images/Background.png');
		this.load.image(KEY_TITLE, 'assets/Images/Title.png')


		this.load.image(KEY_START, 'assets/Images/Play.png')
		this.load.image(KEY_SETTINGS, 'assets/Images/Settings.png')
		this.load.image(KEY_LEADERBOARD, 'assets/Images/LeaderBoard.png')

		//SFX
		this.load.audio('click', 'assets/SFX/ButtonClick.wav');

	}


}



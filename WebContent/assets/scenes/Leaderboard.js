

const KEY_BACKGROUND = 'Background'
const KEY_TITLE = 'Title'
const KEY_RETURN = 'Return'
const KEY_LEADERBOARD = 'LeaderboardArt'

var txtInfo

class Leaderboard extends Phaser.Scene {

	constructor() {

		super("Leaderboard");

	}

	create() {				
		const x = 400
		const y = 300


		var bgIMG = this.add.image(x, 200, KEY_BACKGROUND)


		var txtHighScore = this.add.text(287, 70, 'score: 0', { fontSize: '36px', fill: '#FE00F2', boundsAlignH: "center", boundsAlignV: "middle", align: "center" });
		txtHighScore.setText("High Scores")

		txtInfo = this.add.text(310, 120, 'score: 0', { fontSize: '24px', fill: '#FFFFFF', boundsAlignH: "center", boundsAlignV: "middle", align: "center" });
		var txtOutput = ''
		var LeaderBoardScores = GetLeaderBoardData()
		
		
		LeaderBoardScores.forEach(s => txtOutput += s.key + ' - ' + s.value + '\n'); 
		txtInfo.setText(txtOutput)
		

		//Return Button
		var ReturnButton = this.add.image(x, 385, KEY_RETURN)
		ReturnButton.setTint(0x686868)
		ReturnButton.setInteractive()

		ReturnButton.on('pointerdown', () => this.scene.start('MainMenu'))
		ReturnButton.on('pointerover', function() {
			this.clearTint()
		})
		ReturnButton.on('pointerout', function() {

			this.setTint(0x686868);
		})


	}


	preload() {


		this.load.image(KEY_BACKGROUND, 'assets/Images/Background.png');
		this.load.image(KEY_TITLE, 'assets/Images/Title.png')
		this.load.image(KEY_RETURN, 'assets/Images/Return.png')



	}


}





const KEY_BACKGROUND = 'Background'
const KEY_TITLE = 'Title'
const KEY_SETTINGS = 'Settings'
const KEY_RETURN = 'Return'
const KEY_SOUND_ON = 'SoundOn'
const KEY_SOUND_OFF = 'SoundOff'
const KEY_LEADERBOARD = 'Leaderboard'

var ToggleSoundOn
var ToggleSoundOff

var click

class Settings extends Phaser.Scene {

	constructor() {

		super("Settings");

	}

	create() {
		click = this.sound.add('click');
		var pointer = this.input.activePointer;


		const x = 400
		const y = 300


		var bgIMG = this.add.image(x, 200, KEY_BACKGROUND)

		//TOGGLE SOUND ON
		ToggleSoundOn = this.add.image(x, 250, KEY_SOUND_ON)
		ToggleSoundOn.setTint(0x686868)
		ToggleSoundOn.setInteractive()


		ToggleSoundOn.on('pointerdown', function(event) {
			ToggleSound(true)
			ToggleSoundOn.visible = false;
			ToggleSoundOff.visible = true;
		})




		ToggleSoundOn.on('pointerover', function() {
			this.clearTint()
		})
		ToggleSoundOn.on('pointerout', function() {

			this.setTint(0x686868);
		})



		//TOGGLE SOUND OFF
		ToggleSoundOff = this.add.image(x, 250, KEY_SOUND_OFF)
		ToggleSoundOff.setTint(0x686868)
		ToggleSoundOff.setInteractive()
		ToggleSoundOff.visible = false;


		ToggleSoundOff.on('pointerdown', function(event) {
			ToggleSound(false)
			ToggleSoundOn.visible = true;
			ToggleSoundOff.visible = false;
			click.play()

		})
		ToggleSoundOff.on('pointerover', function() {
			this.clearTint()
		})
		ToggleSoundOff.on('pointerout', function() {

			this.setTint(0x686868);
		})





		var ReturnButton = this.add.image(x, 385, KEY_RETURN)
		ReturnButton.setTint(0x686868)
		ReturnButton.setInteractive()

		//Return Button
		ReturnButton.on('pointerdown', () => this.scene.start('MainMenu'), click.play())
		ReturnButton.on('pointerover', function() {
			this.clearTint()
		})
		ReturnButton.on('pointerout', function() {

			this.setTint(0x686868);
		})

	}

	preload() {

		this.load.image(KEY_BACKGROUND, 'assets/Images/Background.png');
		this.load.image(KEY_TITLE, 'assets/Images/Title.png')


		this.load.image(KEY_RETURN, 'assets/Images/Return.png')
		this.load.image(KEY_SOUND_ON, 'assets/Images/SoundOn.png')
		this.load.image(KEY_SOUND_OFF, 'assets/Images/SoundOff.png')

		this.load.audio('click', 'assets/SFX/ButtonClick.wav');
	}


}



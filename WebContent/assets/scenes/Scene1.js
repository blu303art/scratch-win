
const KEY_BACKGROUND = 'Background'
const KEY_TITLE = 'Title'
const KEY_SCORE = 'Score'
const KEY_PHASER_LOGO = 'phaser-logo'
const KEY_BRUSH = 'Brush'
const KEY_SCRATCH_GOLD = 'Scratch'
const KEY_SCRATCH_CARD = 'Backing'
const KEY_SCOREBOARD = 'ScoreBoard'
const KEY_RETRY = 'Retry'

const KEY_BELL = 'Bell'
const KEY_CLOVER = 'Clover'
const KEY_CLUB = 'Club'
const KEY_CROWN = 'Crown'
const KEY_DIAMOND = 'Diamond'
const KEY_HEART = 'Heart'
const KEY_LEMON = 'Lemon'
const KEY_GIFT = 'Gift'
const KEY_BOMB = 'Bomb'

//collections 
var Bells = []
var Clovers = []
var Clubs = []
var Crowns = []
var Diamonds = []
var Hearts = []
var Lemons = []
var Gifts = []
var Bombs = []

var images = [KEY_BELL, KEY_CLOVER, KEY_CLUB, KEY_HEART, KEY_DIAMOND, KEY_LEMON, KEY_GIFT, KEY_BOMB, KEY_BOMB]
//var images = [KEY_DIAMOND, KEY_BOMB] //FOR TESTING


var score
var txtScore
var txtInfo
var txtMatch
var txtHowToPlay

var RetryButton


var iLives = 3
var Heart1
var Heart2
var Heart3

//SFX
var click
var gameover
var match
var explode
var nomatch
var diamondmatch

var txtOutput
var pointer
var iRevealed

var RetryButton
var currScene

const x = 400
const y = 300

var TimeLines
var Particles
var particles
var container



MatchCheck = function(name, arr) {

	var len = arr.length
	var Modifier = 10

	if (name == 'Diamonds') {
		Modifier = 30
	}

	if (len >= 3) {
		txtOutput += len + ' ' + name + ' found!\n'
		if (name != 'Bombs') {
			score += Modifier * len
		}
	}

	for (let i = 0; i < len; i++) {

		arr[i].on('pointerover', function() {

			if (pointer.isDown) {

				this.disableInteractive()

				var timeline = currScene.tweens.createTimeline();


				if (len >= 3) {
					timeline.add({
						targets: this,
						scaleX: 0,
						duration: 150,
						ease: 'Linear',
						yoyo: true,

					})

					timeline.add({
						targets: this,
						scaleX: .25,
						scaleY: .25,
						ease: 'Sine.easeInOut',
						duration: 200,
						yoyo: true,

					})

					if (name == 'Diamonds') {

						var GridPos = GetGridPos(this.getData('GridID'))
						particles = currScene.add.particles('flares');
						container.add(particles)

						particles.createEmitter({
							frame: ['silver', 'yellow'],
							x: GridPos[0],
							y: GridPos[1],
							lifespan: 700,
							speed: 50,
							scale: { start: 0.30, end: 0 },
							blendMode: 'SCREEN'
						});

						particles.setVisible(false);
						Particles.push(particles)

					}

				} else {

					timeline.add({
						targets: this,
						alpha: 0.25,
						duration: 250,
					})

				}

				TimeLines.push(timeline)

				this.clearTint()
				iRevealed += 1

				Reveal()

			}

		});

	}

}


Reveal = function() {

	if (iRevealed >= 8) {

		SetScore(score)
		RefreshLives()

		if (txtOutput.length == 0) {
			txtInfo.setText('No matches found.')
			nomatch.play()
		} else {
			txtInfo.setText(txtOutput)

			if (Bombs.length >= 3) {
				explode.play()
				ConsumeLife()
				RefreshLives()
			}

			if (Hearts.length >= 3) {
				AddLife()
				RefreshLives()
			}

			if (Diamonds.length >= 3) {
				diamondmatch.play()
			} else {
				match.play()
			}
		}


		//Reveal match animations 
		TimeLines.forEach(t => t.play())

		//enable particle effects 
		Particles.forEach(p => p.setVisible(true));

		//higher order function example 		
		GameOverCheck(GetLives(), GameOver, Continue);

	}
}


GameOverCheck = function(lives, over, next) {

	if (lives <= 0) {
		gameover.play();
		GameOver(score);
		over(score)

	} else {
		next();
	}

}

Continue = function() {

	txtScore.setText(score);
	RetryButton.setInteractive()
	RetryButton.clearTint()
}


RefreshLives = function() {

	iLives = GetLives()

	if (iLives >= 1) {
		Heart1.visible = true
	} else {
		Heart1.visible = false
	}

	if (iLives >= 2) {
		Heart2.visible = true
	} else {
		Heart2.visible = false
	}

	if (iLives == 3) {
		Heart3.visible = true
	} else {
		Heart3.visible = false
	}

}

SetupLives = function() {

	iLives = GetLives()

	Heart1 = currScene.add.image(770, 30, KEY_HEART).setDisplaySize(70, 70)
	Heart2 = currScene.add.image(720, 30, KEY_HEART).setDisplaySize(70, 70)
	Heart3 = currScene.add.image(670, 30, KEY_HEART).setDisplaySize(70, 70)

	RefreshLives()
}



GetGridPos = function(i) {
	var xPos = 0
	var YPos = 0


	if (i == 0) {
		xPos = -90
		YPos = -30
	} else if (i == 1) {
		xPos = -30
		YPos = -30
	} else if (i == 2) {
		xPos = 30
		YPos = -30
	} else if (i == 3) {
		xPos = 90
		YPos = -30
	} else if (i == 4) {
		xPos = -90
		YPos = 30
	} else if (i == 5) {
		xPos = -30
		YPos = 30
	}
	else if (i == 6) {
		xPos = 30
		YPos = 30
	} else if (i == 7) {
		xPos = 90
		YPos = 30
	}

	var oRet = [xPos, YPos]

	return oRet
}

SetupSFX = function() {
	click = currScene.sound.add('click')
	match = currScene.sound.add('match')
	gameover = currScene.sound.add('gameover')
	explode = currScene.sound.add('explode')
	nomatch = currScene.sound.add('nomatch')
	diamondmatch = currScene.sound.add('diamondmatch')
}

GenerateSymbols = function() {


	for (let i = 0; i < 8; i++) {
		var xPos = 0
		var YPos = 0

		var GridPos = GetGridPos(i)
		xPos = GridPos[0]
		YPos = GridPos[1]

		var type = Phaser.Math.RND.pick(images)
		var img = currScene.add.image(0, 0, type)

		img.text = type
		img.displayWidth = 60
		img.displayHeight = 60
		container.add(img)
		img.setPosition(xPos, YPos)
		img.setTint(0x686868);

		img.setInteractive()
		img.setData('GridID', i)

		switch (type) {
			case 'Bell':
				Bells.push(img)
				break;
			case 'Clover':
				Clovers.push(img)
				break;
			case 'Club':
				Clubs.push(img)
				break;
			case 'Crown':
				Crowns.push(img)
				break;
			case 'Diamond':
				Diamonds.push(img)
				break;
			case 'Heart':
				Hearts.push(img)
				break;
			case 'Lemon':
				Lemons.push(img)
				break;
			case 'Gift':
				Gifts.push(img)
				break;
			case 'Bomb':
				Bombs.push(img)
				break;

		}

	}

	MatchCheck("Bells", Bells)
	MatchCheck("Clovers", Clovers)
	MatchCheck("Clubs", Clubs)
	MatchCheck("Crowns", Crowns)
	MatchCheck("Diamonds", Diamonds)
	MatchCheck("Hearts", Hearts)
	MatchCheck("Lemons", Lemons)
	MatchCheck("Gifts", Gifts)
	MatchCheck("Bombs", Bombs)

}




class Scene1 extends Phaser.Scene {





	constructor() {

		super("Scene1");

		this.isDown = false
		this.cover = null
		this.renderTexture = null

	}



	create() {


		currScene = this
		iRevealed = 0
		pointer = this.input.activePointer;
		txtOutput = ''

		//clear collections 
		Bells = []
		Clovers = []
		Clubs = []
		Crowns = []
		Diamonds = []
		Hearts = []
		Lemons = []
		Gifts = []
		Bombs = []
		TimeLines = []
		Particles = []


		//SFX
		SetupSFX()


		//iLives = GetLives()
		score = GetScore()

		//set background & title images 
		var bgIMG = this.add.image(x, 200, KEY_BACKGROUND)
		var Title = this.add.image(x, 100, KEY_TITLE)

		RetryButton = this.add.image(700, 400, KEY_RETRY)
		RetryButton.on('pointerdown', () => this.scene.restart(), click.play(), IncrementAttempts())
		RetryButton.setTint(0x686868);

		SetupLives()

		//Add score and backing score board
		var ScoreBoard = this.add.image(150, 130, KEY_SCOREBOARD)
		Title.setDisplaySize(256, 256)
		txtScore = this.add.text(60, 125, score, { fontSize: '36px', fill: '#FE00F2', boundsAlignH: "center", boundsAlignV: "middle", align: "center" });
		txtInfo = this.add.text(320, 410, '', { fontSize: '18px', fill: '#FFFFFF', boundsAlignH: "center", boundsAlignV: "middle", align: "center" });


		var txt = ""
		//only show this to the player once. 
		if (GetAttempts() == 1) {
			txt = "3 or more make a match - Scratch and earn poins. \nbut avoid the bombs, 3 or more will take a life. goodluck!"
			txtHowToPlay = this.add.text(170, 410, txt, { fontSize: '14px', fill: '#FFFFFF', boundsAlignH: "center", boundsAlignV: "middle", align: "center" });
		}


		var reveal = this.add.image(x, y, KEY_SCRATCH_CARD)
		container = this.add.container(400, 300);

		GenerateSymbols()


		this.cover = this.add.image(x, y, KEY_SCRATCH_GOLD)
		const width = this.cover.width
		const height = this.cover.height

		const rt = this.make.renderTexture({
			width,
			height,
			add: false
		})

		const maskImage = this.make.image({
			x,
			y,
			key: rt.texture.key,
			add: false
		})


		this.cover.mask = new Phaser.Display.Masks.BitmapMask(this, maskImage)
		this.cover.mask.invertAlpha = true

		reveal.mask = new Phaser.Display.Masks.BitmapMask(this, maskImage)

		this.cover.setInteractive()
		this.cover.on(Phaser.Input.Events.POINTER_DOWN, this.handlePointerDown, this)
		this.cover.on(Phaser.Input.Events.POINTER_MOVE, this.handlePointerMove, this)
		this.cover.on(Phaser.Input.Events.POINTER_UP, () => this.isDown = false)

		this.brush = this.make.image({
			key: KEY_BRUSH,
			add: false
		})

		this.renderTexture = rt

	}




	preload() {

		this.input.topOnly = false

		//load sprites
		this.load.image(KEY_BACKGROUND, 'assets/Images/Background.png');
		this.load.image(KEY_TITLE, 'assets/Images/Title.png');
		this.load.image(KEY_SCORE, 'assets/Images/Score.png');
		this.load.image(KEY_BRUSH, 'assets/Images/Brush2.png')
		this.load.image(KEY_SCRATCH_GOLD, 'assets/Images/Scratch.png')
		this.load.image(KEY_SCRATCH_CARD, 'assets/Images/Card.png')
		this.load.image(KEY_SCOREBOARD, 'assets/Images/ScoreBoard.png')
		this.load.image(KEY_RETRY, 'assets/Images/Retry.png')

		this.load.image(KEY_BELL, 'assets/Images/Bell.png')
		this.load.image(KEY_CLOVER, 'assets/Images/Clover.png')
		this.load.image(KEY_CLUB, 'assets/Images/Club.png')
		this.load.image(KEY_CROWN, 'assets/Images/Crown.png')
		this.load.image(KEY_DIAMOND, 'assets/Images/Diamond.png')
		this.load.image(KEY_HEART, 'assets/Images/Heart.png')
		this.load.image(KEY_LEMON, 'assets/Images/Lemon.png')
		this.load.image(KEY_GIFT, 'assets/Images/Gift.png')
		this.load.image(KEY_BOMB, 'assets/Images/Bomb.png')


		//load particle 		
		this.load.atlas('flares', 'assets/Particles/flares.png', 'assets/Particles/flares.json');

		//load audio effects
		this.load.audio('click', 'assets/SFX/ButtonClick.wav')
		this.load.audio('match', 'assets/SFX/MatchFound.wav')
		this.load.audio('nomatch', 'assets/SFX/NoMatch.wav')
		this.load.audio('gameover', 'assets/SFX/GameOver.wav')
		this.load.audio('explode', 'assets/SFX/Explode.wav')
		this.load.audio('diamondmatch', 'assets/SFX/DiamondMatch.wav')
	}






	update() {



	}







	handlePointerOver(pointer) {

	}

	handlePointerDown(pointer) {
		txtHowToPlay.visible = false
		this.isDown = true
		this.handlePointerMove(pointer)
	}


	handlePointerMove(pointer) {
		if (!this.isDown) {
			return
		}

		const x = pointer.x - this.cover.x + this.cover.width * 0.5
		const y = pointer.y - this.cover.y + this.cover.height * 0.5
		this.renderTexture.draw(this.brush, x, y)
	}

}




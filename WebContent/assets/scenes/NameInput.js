

var currScene
const KEY_BACKGROUND = 'Background'
const KEY_TITLE = 'Title'
const KEY_RETURN = 'Return'
const KEY_LEADERBOARD = 'LeaderboardArt'

var click


class NameInput extends Phaser.Scene {


	constructor() {

		super("NameInput");

	}


	preload() {

		this.load.image(KEY_BACKGROUND, 'assets/Images/Background.png');
		this.load.image(KEY_TITLE, 'assets/Images/Title.png')
		this.load.image(KEY_RETURN, 'assets/Images/Return.png')

		this.load.image('block', 'assets/Input/block.png');
		this.load.image('rub', 'assets/Input/rub.png');
		this.load.image('end', 'assets/Input/end.png');
		this.load.bitmapFont('arcade', 'assets/Fonts/bitmap/arcade.png', 'assets/Fonts/bitmap/arcade.xml');

		this.load.image('donebutton', 'assets/Images/Done.png')

		//SFX
		this.load.audio('click', 'assets/SFX/ButtonClick.wav');
	}




	create() {
		currScene = this
		click = this.sound.add('click');

		const x = 400
		const y = 300


		var bgIMG = this.add.image(x, 200, KEY_BACKGROUND)
		var chars = [
			['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
			['K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'],
			['U', 'V', 'W', 'X', 'Y', 'Z', '.', '-', '<', '>']
		]
		var cursor = { x: 0, y: 0 }
		var name = ''

		var input = this.add.bitmapText(130, 50, 'arcade', 'ABCDEFGHIJ\n\nKLMNOPQRST\n\nUVWXYZ.-').setLetterSpacing(20).on('pointerdown', function() { click.play() })

		input.setInteractive()

		var donebutton = this.add.image(700, 400, 'donebutton').setInteractive().setTint(0x686868)		
		donebutton.on('pointerdown', function() {
			click.play()
			SetPlayerName(name)
			currScene.scene.start('Leaderboard')
		})


		donebutton.on('pointerover', function() {
			this.clearTint()
		})
		donebutton.on('pointerout', function() {

			this.setTint(0x686868);
		})


		var rub = this.add.image(input.x + 430, input.y + 148, 'rub')
		var end = this.add.image(input.x + 482, input.y + 148, 'end')

		var block = this.add.image(input.x - 10, input.y - 2, 'block').setOrigin(0)
		this.add.bitmapText(80, 310, 'arcade', 'Your name: ').setTint(0xff0000)

		var playerText = this.add.bitmapText(400, 310, 'arcade', name).setTint(0xff0000)

		this.input.keyboard.on('keyup', function(event) {

			if (event.keyCode === 37) {
				//  left
				if (cursor.x > 0) {
					cursor.x--;
					block.x -= 52;
				}
			}
			else if (event.keyCode === 39) {
				//  right
				if (cursor.x < 9) {
					cursor.x++;
					block.x += 52;
				}
			}
			else if (event.keyCode === 38) {
				//  up
				if (cursor.y > 0) {
					cursor.y--;
					block.y -= 64;
				}
			}
			else if (event.keyCode === 40) {
				//  down
				if (cursor.y < 2) {
					cursor.y++;
					block.y += 64;
				}
			}
			else if (event.keyCode === 13 || event.keyCode === 32) {
				//  Enter or Space
				if (cursor.x === 9 && cursor.y === 2 && name.length > 0) {
					//  Submit
				}
				else if (cursor.x === 8 && cursor.y === 2 && name.length > 0) {
					//  Rub
					name = name.substr(0, name.length - 1);

					playerText.text = name;
				}
				else if (name.length < 10) {
					//  Add
					name = name.concat(chars[cursor.y][cursor.x]);

					playerText.text = name;
				}
			}

		});

		input.on('pointermove', function(pointer, x, y) {

			var cx = Phaser.Math.Snap.Floor(x, 52, 0, true);
			var cy = Phaser.Math.Snap.Floor(y, 64, 0, true);
			var char = chars[cy][cx];

			cursor.x = cx;
			cursor.y = cy;

			block.x = input.x - 10 + (cx * 52);
			block.y = input.y - 2 + (cy * 64);

		}, this);

		input.on('pointerup', function(pointer, x, y) {

			var cx = Phaser.Math.Snap.Floor(x, 52, 0, true);
			var cy = Phaser.Math.Snap.Floor(y, 64, 0, true);
			var char = chars[cy][cx];

			cursor.x = cx;
			cursor.y = cy;

			block.x = input.x - 10 + (cx * 52);
			block.y = input.y - 2 + (cy * 64);

			if (char === '<' && name.length > 0) {
				//  Rub
				name = name.substr(0, name.length - 1);

				playerText.text = name;
			}
			else if (char === '>' && name.length > 0) {
				//  Submit
			}
			else if (name.length < 10) {
				//  Add
				name = name.concat(char);

				playerText.text = name;
			}

		}, this);

	}




}


import Phaser from 'phaser'

const KEY_PHASER_LOGO = 'phaser-logo'
const KEY_BRUSH = 'brush'

export default class RevealMaskScene extends Phaser.Scene
{
	constructor()
	{
		super('reveal-mask')

		this.cover = null
	}

	preload()
	{
		this.load.image(KEY_PHASER_LOGO, 'assets/phaser-logo.png')
		this.load.image(KEY_BRUSH, 'assets/brush.png')
	}

	create()
	{
		const x = 400
		const y = 300

		const reveal = this.add.image(x, y, KEY_PHASER_LOGO)
		this.cover = this.add.image(x, y, KEY_PHASER_LOGO)
		this.cover.setTint(0x4bf542)
	}
}
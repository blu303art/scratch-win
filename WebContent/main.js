


window.addEventListener('load', function() {
	var PlayerScore = 0
	var LeaderBoardScores = [{ key: 'Luke', value: 1650 }, { key: 'Kat', value: 2600 }, { key: 'Richard', value: 2350 }]
	var PlayerLives = 3
	var Attempts = 0
	var PlayerName = ''

	var game = new Phaser.Game({
		"title": "ScratchGame",
		"width": 800,
		"height": 450,
		"type": Phaser.AUTO,
		"backgroundColor": "#88F",
		"parent": "game-container",
		"scale": {
			"mode": Phaser.Scale.FIT,
			"autoCenter": Phaser.Scale.CENTER_BOTH
		}
	});
	game.scene.add("Boot", Boot, true);




	GameOver = function(score) {
		PlayerScore = score;

		if (PlayerName.length == 0) {
			game.scene.start("NameInput")
		} else {
			LeaderBoardScores.push({ key: PlayerName, value: score })
			game.scene.start("Leaderboard")
		}

	}

	GetLives = function() {
		return PlayerLives
	}

	ConsumeLife = function() {
		PlayerLives -= 1
	}

	AddLife = function() {
		if (PlayerLives < 3)
			PlayerLives += 1
	}

	ResetLives = function() {
		PlayerScore = 0
		PlayerLives = 3
	}


	GetScore = function() {
		return PlayerScore
	}

	SetScore = function(score) {
		PlayerScore = score
	}

	GetLeaderBoardData = function() {

		return LeaderBoardScores.sort((a, b) => b.value - a.value)
	}

	IncrementAttempts = function() {
		Attempts += 1
	}

	GetAttempts = function() {
		return Attempts
	}



	ToggleSound = function(enabled) {

		game.sound.setMute(enabled)

	}

	SetPlayerName = function(name) {


		if (name.length == 0) {
			PlayerName = 'You'
		} else {
			PlayerName = name;
		}

		LeaderBoardScores.push({ key: PlayerName, value: PlayerScore })

	}

	GetPlayerName = function() {
		return PlayerName;
	}


});



class Boot extends Phaser.Scene {

	preload() {
		this.load.pack("pack", "assets/pack.json");
	}

	create() {
		//this.scene.start("Scene1");
		this.scene.start("MainMenu");
	}

}
